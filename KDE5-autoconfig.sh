#!/bin/bash

# Tested only on KDE5

#TODO: Write ansible module
# Read
kreadconfig5 --file kxkbrc --group Layout --key Options
# Write
kwriteconfig5 --file kxkbrc --group Layout --key Options "grp:caps_toggle,grp_led:scroll,compose:lwin"


# Check if we have working environment
# Check env var XDG_CURRENT_DESKTOP=KDE and KDE_SESSION_VERSION=5

# Reload config in working env
dbus-send /Layouts org.kde.keyboard.reloadConfig


# Articles
#https://kubuntu.ru/node/14497



#TODO:
kwriteconfig5 --file kmail2rc --group General --key SystemTrayEnabled true



# Disable some default effects
kwriteconfig5 --file kwinrc --group Plugins --key slideEnabled false
qdbus org.kde.KWin /Effects org.kde.kwin.Effects.unloadEffect slide

kwriteconfig5 --file kwinrc --group Plugins --key kwin4_effect_fade false
qdbus org.kde.KWin /Effects org.kde.kwin.Effects.unloadEffect kwin4_effect_fade
